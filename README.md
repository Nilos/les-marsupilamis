Test Marsupilamis : 

L'objectif est de créer un carnet d'adresse pour marsupilami(animal fictif) sous symfony à savoir:

(Inscription/Connexion/Déconnexion) du marsupilami par login/mot de passe (FOSUserbundle)

(Afficher/Modifier) ses informations (age / famille / race / nourriture)

(Ajouter/Supprimer) un marsupilami de sa liste d'amis (relation "many to many" sur une seule table)

